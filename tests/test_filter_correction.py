import pytest


@pytest.mark.parametrize(
    "has_medium_filter_eta,"
    "has_fine_filter_sup,"
    "expected",
    [
        (True, True, 0),
        (True, False, 190),
        (False, False, 340),
        (False, True, 150),
    ],
)
def test_filter_correction(
    has_medium_filter_eta,
    has_fine_filter_sup,
    expected,
):
    from erp_air.erp_utils import filters_correction

    assert (
        filters_correction(
            has_medium_filter_eta, has_fine_filter_sup
        )
        == expected
    )
