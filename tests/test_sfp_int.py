import pytest


@pytest.mark.parametrize(
    "unit_type,"
    "hrs_type,"
    "nominal_airflow,"
    "hrs_eff_bonus,"
    "filters_correction,"
    "has_fine_filter,"
    "expected,",
    # fmt:off
    [
        ("bvu", "rw", 0, 0, 0, 0, 1100),
        ("bvu", "rw", 1, 100, 150, 0, 900),
        ("bvu", "phex", 2, 0, 0, 0, 800),
        ("bvu", "phex", 2, 100, 150, 0, 750),
        ("bvu", "rac", 2, 100, 150, 0, 1250),
        ("uvu", None, 2, 100, 150, 1, 230),
        pytest.param(
            "uvu", None, 2, 100, 150, 0, 1000,
            marks=pytest.mark.xfail(raises=ValueError),
        ),
    ],
    # fmt:on
)
def test_sfp_int_limit(
    unit_type,
    hrs_type,
    nominal_airflow,
    hrs_eff_bonus,
    filters_correction,
    has_fine_filter,
    expected,
):
    from erp_air.erp_utils import sfp_int_limit

    assert (
        sfp_int_limit(
            unit_type,
            hrs_type,
            nominal_airflow,
            hrs_eff_bonus,
            filters_correction,
            has_fine_filter,
        )
        == expected
    )


@pytest.mark.parametrize(
    "unit_type,has_fine_filter_sup,expected",
    [
        ("bvu", True, True),
        ("bvu", False, True),
        ("uvu", True, True),
        ("uvu", False, False),
    ],
)
def test_sfp_validation(
    unit_type, has_fine_filter_sup, expected
):
    from erp_air.erp_utils import sfp_validation

    assert (
        sfp_validation(unit_type, has_fine_filter_sup)
        == expected
    )


@pytest.mark.parametrize(
    "pressure_drop_int_vent_comps,"
    "fan_eff_system_static,"
    "expected",
    [(50, 0.3, 166), (100, 0.4, 250)],
)
def test_sfp_int(
    pressure_drop_int_vent_comps,
    fan_eff_system_static,
    expected,
):
    from erp_air.erp_utils import sfp_int

    assert (
        sfp_int(
            pressure_drop_int_vent_comps,
            fan_eff_system_static,
        )
        == expected
    )
